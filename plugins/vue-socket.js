import Vue from 'vue'
import socketio from 'socket.io-client'
import VueSocketIO from 'vue-socket.io'

Vue.use(new VueSocketIO({
  debug: true,
  connection: socketio(
    'http://localhost:8300/notifications',
    { transports: ['websocket', 'polling'] }
  ),
})
)
