module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    jest: true,
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
    'plugin:nuxt/recommended',
    'prettier',
  ],
  plugins: [],
  rules: {
    'no-multiple-empty-lines': ['error', {
      'max': 1,
      'maxEOF': 1,
    }],
    'semi': ['error', 'never'],
    'quotes': ['error', 'single'],
    'eol-last': 'error',
    'new-parens': 'error',
    'no-return-await': 'error',
    'no-trailing-spaces': 'error',
    'prefer-template': 'error',
    'no-multi-spaces': 'error',
    'comma-dangle': ['error', {
      'arrays': 'always-multiline',
      'objects': 'always-multiline',
      'imports': 'always-multiline',
      'exports': 'always-multiline',
      'functions': 'never',
    }],
    'vue/html-closing-bracket-newline': ['error', {
      'singleline': 'never',
      'multiline': 'never',
    }],
    'vue/multi-word-component-names': ['off'],
  },
}
