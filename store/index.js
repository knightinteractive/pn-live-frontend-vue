
export const state = () => ({
  activeChannel: null,
  channels: [],
  socket: {},
})

export const actions = {
  async fetchChannels({ commit }) {
    const channels = await fetch(
      'https://capi.pn-internals.net/channels'
    ).then((res) => res.json())

    commit('setChannels', channels.map((channel) => {
      return {
        rid: channel.rid,
        slug: channel.slug,
        logo: channel.logo,
        title: channel.title,
      }
    }))
  },

  async fetchChannel({ commit }, slug) {
    const channel = await fetch(
      `https://capi.pn-internals.net/channels/${slug}`
    ).then((res) => res.json())
    commit('setActiveChannel', channel)
    return channel
  },

  connectSocket({ commit }) {
    commit('setSocket', this.$nuxtSocket({
      channel: '/notifications',
      withCredentials: true,
    }))
  },

  updateChannelSchedule({ commit }, schedule) {
    commit('updateChannelSchedule', schedule)
  },
}

export const mutations = {
  updateChannelSchedule(state, schedule) {
    const { currentPlay, onDeck } = schedule
    state.activeChannel = {
      ...state.activeChannel,
      currentPlay,
      onDeck,
    }
  },

  setActiveChannel(state, channel) {
    state.activeChannel = channel
  },
  setChannels(state, value) {
    state.channels = value
  },
  setSocket(state, value) {
    state.socket = value
  },
}
