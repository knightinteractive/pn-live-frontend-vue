export const state = () => ({
  userAgent: '',
  isMobile: false,
  isTablet: false,
})

export const actions = {
  setUserAgent({commit}, navigator) {
    commit('setUserAgent', navigator.userAgent.toLowerCase())
  },

  setDevice({commit, state}) {
    commit('setDevice', {
      isMobile:
        /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(
          state.userAgent
        ),
      isTablet:
        /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(
          state.userAgent
        ),
    })
  },
}

export const mutations = {
  setUserAgent(state, value){
    state.userAgent = value
  },

  setDevice(state, value){
    state.isMobile = value.isMobile
    state.isTablet = value.isTablet
  },
}
